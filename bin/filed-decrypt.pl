#!/usr/bin/perl

#----------------------------------------------------------------------#
# $Id: filed,v 0.1 2003/03/18 15:00:00 swilson@ndchealth.com Exp $    #
# $Log: filed,v $                                                      #
#----------------------------------------------------------------------#

# -- @INC needs our local lib
BEGIN {
  use lib '/opt/filed/lib';
}

die "usage: decrypt X0OqHb8cDoabMdQYp71MC2Del2rdm89+frdn/jq4q7zryhUZeZb7jGWQTXcMlMUz\n" unless @ARGV eq 1;

# -- our package name
package Filed;

# -- strict, warnings
use strict;
use warnings;

# -- required packages
use Crypt::Simple;

# -- turn off buffering
$|=1;

print "\nDecrypted password is:\n\n",decrypt($ARGV[0]),"\n\n";
