# --
# -- spec file for filed
# --

# -- don't strip binary par files, only compress
%define __os_install_post /usr/lib/rpm/brp-compress

Summary: A utility for unattended batch file processing.
Name: filed
Version: 1.42
Release: 1
License: GPL
Group: System Environment/Tools
Source: %{name}-%{version}.tgz

URL: http://mendeni.com/filed.html
Vendor: Mendeni, LLC
Packager: swilson@mendeni.com

Requires: gnupg expat
BuildRequires: gnupg perl >= 1:5.6.0 which
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root

%description
Filed supports FTP and SFTP file transfer protocols for posts and gets along with these file operations: gzip, gunzip, zip, unzip, sh, PGP/GPG encrypt, PGP/GPG decrypt. The encrypt/decrypt commands require GnuPG. FTP and SFTP protocols are supported via the Net::FTP and Net::SFTP modules which are bundled with the script for Linux deployments.

%package admin
Group: System Environment/Tools
Summary: Admin tools for use with filed.
Requires: filed

%description admin
Provides the filed encrypt and decrypt utilities for use in filed <password> tag. This package should generally not be installed on production systems where filed will run, but rather on management systems where passwords can be more safely maintained.

%prep
rm -r -f -v %{buildroot}
%setup

%install
./configure

mkdir -p -v %{buildroot}%{_sbindir}
mkdir -p -v %{buildroot}%{_mandir}/man5

%{__install} -p -m0755 bin/filed %{buildroot}%{_sbindir}
%{__install} -p -m0755 bin/filed-encrypt %{buildroot}%{_sbindir}
%{__install} -p -m0755 bin/filed-decrypt %{buildroot}%{_sbindir}
%{__install} -p -m0644 bin/filed.man %{buildroot}%{_mandir}/man5/filed.5

%clean
rm -rf %{buildroot}

%files
%defattr(-, root, root)
%{_sbindir}/filed
%doc %{_mandir}/man5/filed.5.gz

%files admin
%defattr(-, root, root)
%{_sbindir}/filed-encrypt
%{_sbindir}/filed-decrypt

%changelog
* Wed Jan 28 2009 Steve Wilson <swilson@mendeni.com>
- Updated to 1.42 to support recursive ftp get.

* Fri Dec 19 2008 Steve Wilson <swilson@mendeni.com>
- Updated to 1.41 which logs when ftp_get size fails to compare

* Sat Nov 08 2008 Steve Wilson <swilson@mendeni.com>
- First build of filed 1.40.

