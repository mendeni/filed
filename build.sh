#!/bin/bash -xe

# install packaging tools and deps
yum -y install rpm-build gnupg make gcc perl-XML-Parser perl-XML-LibXML perl-libwww-perl perl-CPAN perl-YAML which

# install perl deps via cpan
export PERL_MM_USE_DEFAULT=1;
export PERL_MM_NONINTERACTIVE=1;
export AUTOMATED_TESTING=1;

# loop thru each required module and install
for p in Module::Runtime Module::Implementation Test::Fatal Attribute::Handlers Params::Validate XML::DT Crypt::Simple GnuPG Net::SFTP Net::FTP::Recursive Net::SFTP Net::SFTP::Util Mail::Sendmail MIME::QuotedPrint MIME::Base64 Proc::Queue Proc::ProcessTable Log::Dispatch::Syslog Linux::Fuser pp
do
  echo "<<<<<<<<<<<<<<<<<<< $p >>>>>>>>>>>>>>>>>>>>>"
  perl -MCPAN -e "install $p"
done

# fetch filed src.rpm
rpm -i filed-1.42-1.src.rpm

# cd to /usr/src/redhat/SPECS and build
cd /usr/src/redhat/SPECS && \
rpmbuild -ba filed.spec

# sanity check on filed.pl
perl -c /usr/src/redhat/BUILD/filed-1.42/bin/filed.pl

# here's the packages
find /usr/src/redhat/ -name 'filed*.rpm'
